package com.aa.instagramsimpleapp.utils

import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.aa.instagramsimpleapp.activities.CommentsActivity
import com.aa.instagramsimpleapp.adapters.IndicatorAdapter
import com.aa.instagramsimpleapp.adapters.PhotosAdapter
import com.aa.instagramsimpleapp.models.Photo
import com.aa.instagramsimpleapp.models.Post
import com.squareup.picasso.Picasso

object DataBindingFunctions {

    //Load image urls with data binding and picasso from xml
    @JvmStatic
    @BindingAdapter("image_url")
    fun setImageUrl(imageView: ImageView, url: String) {
        Picasso.get().load(url).into(imageView)
    }

    @JvmStatic
    @BindingAdapter("photos")
    fun setPhotosOnViewPager(viewPager2: ViewPager2, photos: ArrayList<Photo>?) {
        if (photos != null)
            viewPager2.adapter = PhotosAdapter(photos)
    }

    @JvmStatic
    @BindingAdapter("showComments")
    fun startCommentsActivity(view: View, post: Post?) {
        if (post != null)
            view.setOnClickListener {
                val intent = Intent(view.context, CommentsActivity::class.java)
                intent.putExtra(Constants.POST_TAG, post)
                view.context.startActivity(intent)
            }
    }

    @JvmStatic
    @BindingAdapter("setRecyclerIndicator", "indicatorViewPager")
    fun setIndicator(
        recyclerView: RecyclerView,
        photos: ArrayList<Photo>?,
        indicatorViewPager: ViewPager2
    ) {
        if (photos != null) {
            recyclerView.layoutManager =
                LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
            val adapter = IndicatorAdapter(indicatorViewPager.adapter?.itemCount ?: 0, recyclerView)
            recyclerView.adapter = adapter
            indicatorViewPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    adapter.selectedPosition = position
                }
            })
        }
    }
}