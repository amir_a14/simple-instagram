package com.aa.instagramsimpleapp.utils

object Constants {
    const val BASE_URL = "https://jsonplaceholder.typicode.com"
    const val POST_TAG = "post"
}