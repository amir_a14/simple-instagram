package com.aa.instagramsimpleapp.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.aa.instagramsimpleapp.R
import com.aa.instagramsimpleapp.adapters.CommentsAdapter
import com.aa.instagramsimpleapp.databinding.ActivityCommentsBinding
import com.aa.instagramsimpleapp.models.Post
import com.aa.instagramsimpleapp.utils.Constants
import com.aa.instagramsimpleapp.viewmodels.CommentsActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_comments.*

@AndroidEntryPoint
class CommentsActivity : AppCompatActivity() {
    //provide viewModel with kotlin ktx extensions
    private val viewModel: CommentsActivityViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val post: Post = intent.getSerializableExtra(Constants.POST_TAG) as Post
        //Provide DataBinding
        val binding =
            DataBindingUtil.setContentView<ActivityCommentsBinding>(
                this,
                R.layout.activity_comments
            )
        binding.viewModel = viewModel
        binding.post = post
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = this

        title = getString(R.string.comments_title)
        actionBar?.setHomeButtonEnabled(true)

        post.id?.let { viewModel.getServerComments(it) }
        viewModel.comments.observe(this, {
            commentsRecyclerView.layoutManager = LinearLayoutManager(this)
            commentsRecyclerView.adapter = if (it != null) CommentsAdapter(it) else null
        })
    }
}