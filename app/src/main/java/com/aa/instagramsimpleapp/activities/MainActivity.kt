package com.aa.instagramsimpleapp.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.aa.instagramsimpleapp.R
import com.aa.instagramsimpleapp.adapters.MainFragmentAdapter
import com.aa.instagramsimpleapp.databinding.ActivityMainBinding
import com.aa.instagramsimpleapp.viewmodels.MainActivityViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    //provide viewModel with kotlin ktx extensions
    private val viewModel: MainActivityViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Provide DataBinding
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = this

        mainViewPager.adapter = MainFragmentAdapter(this)
        setupBottomNavigationWithViewPager()
        mainViewPager.isUserInputEnabled = false
    }

    private fun setupBottomNavigationWithViewPager() {
        val pageIds = listOf(R.id.home, R.id.search, R.id.like, R.id.person)
        val bottomNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener {
            mainViewPager.setCurrentItem(pageIds.indexOf(it.itemId), true)
            true
        }
        bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationListener)
        mainViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                bottomNavigation.setOnNavigationItemSelectedListener(null)
                bottomNavigation.selectedItemId = pageIds[position]
                bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationListener)
            }
        })
    }
}