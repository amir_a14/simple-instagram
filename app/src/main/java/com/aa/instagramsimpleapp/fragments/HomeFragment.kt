package com.aa.instagramsimpleapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aa.instagramsimpleapp.R
import com.aa.instagramsimpleapp.adapters.PostsAdapter
import com.aa.instagramsimpleapp.databinding.FragmentHomeBinding
import com.aa.instagramsimpleapp.viewmodels.HomeFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*

@AndroidEntryPoint
class HomeFragment : Fragment() {
    //provide viewModel with kotlin ktx extensions
    private val viewModel: HomeFragmentViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Provide DataBinding
        val binding = FragmentHomeBinding.bind(view)
        binding.viewModel = viewModel
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.getServerPosts()
        viewModel.posts.observe(viewLifecycleOwner, {
            homeRecyclerView.layoutManager = LinearLayoutManager(context)
            homeRecyclerView.adapter = if (it != null) PostsAdapter(it, viewModel) else null
        })
    }
}