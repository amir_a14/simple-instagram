package com.aa.instagramsimpleapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aa.instagramsimpleapp.R
import kotlinx.android.synthetic.main.item_indicator.view.*

class IndicatorAdapter(private val count: Int, val recyclerView: RecyclerView) :
    RecyclerView.Adapter<IndicatorAdapter.IndicatorViewHolder>() {
    inner class IndicatorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    var selectedPosition = 0
        set(value) {
            val lastPos =
                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            if (value >= lastPos) {
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(if (value == count - 1) value else value + 1)
            }
            val firstPos =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
            if (value <= firstPos) {
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(if (value == 0) value else value - 1)
            }
            notifyItemChanged(field)
            notifyItemChanged(value)
            field = value
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): IndicatorAdapter.IndicatorViewHolder {
        return IndicatorViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_indicator, parent, false)
        )
    }

    override fun onBindViewHolder(holder: IndicatorViewHolder, position: Int) {
        holder.itemView.dot.isSelected = position == selectedPosition
    }

    override fun getItemCount(): Int = count
}