package com.aa.instagramsimpleapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aa.instagramsimpleapp.databinding.ItemPostBinding
import com.aa.instagramsimpleapp.models.Post
import com.aa.instagramsimpleapp.viewmodels.HomeFragmentViewModel


class PostsAdapter(private val posts: ArrayList<Post>, val viewModel: HomeFragmentViewModel) :
    RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        //Provide data binding
        val itemPostBinding =
            ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostsViewHolder(itemPostBinding)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind(posts[position], position)
        viewModel.getServerImages(position)
    }

    override fun getItemCount(): Int = posts.size

    inner class PostsViewHolder(private val postBinding: ItemPostBinding) :
        RecyclerView.ViewHolder(postBinding.root) {

        //Bind view with data binding
        fun bind(post: Post?, position: Int) {
            postBinding.post = post
            postBinding.viewModel = viewModel
            postBinding.position = position
            postBinding.executePendingBindings()
        }
    }
}