package com.aa.instagramsimpleapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aa.instagramsimpleapp.databinding.ItemPhotoBinding
import com.aa.instagramsimpleapp.models.Photo


class PhotosAdapter(private val images: ArrayList<Photo>) :
    RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        //Provide data binding
        val itemPhotoBinding =
            ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotosViewHolder(itemPhotoBinding)
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.bind(images[position])
    }

    override fun getItemCount(): Int = images.size

    class PhotosViewHolder(private val imageBinding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(imageBinding.root) {

        //Bind view with data binding
        fun bind(imageModel: Photo?) {
            imageBinding.setImage(imageModel)
            imageBinding.executePendingBindings()
        }
    }
}