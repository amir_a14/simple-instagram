package com.aa.instagramsimpleapp.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aa.instagramsimpleapp.fragments.HomeFragment
import com.aa.instagramsimpleapp.fragments.SimpleFragment

class MainFragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    val fragments = listOf(
        HomeFragment(),
        SimpleFragment("Search"),
        SimpleFragment("Like"),
        SimpleFragment("Profile"),
    )

    override fun getItemCount(): Int = fragments.size
    override fun createFragment(position: Int): Fragment = fragments[position]

}