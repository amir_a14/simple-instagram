package com.aa.instagramsimpleapp.viewmodels

import android.app.Application
import android.widget.Toast
import androidx.databinding.ObservableArrayMap
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.aa.instagramsimpleapp.models.Photo
import com.aa.instagramsimpleapp.models.Post
import com.aa.instagramsimpleapp.remote.ApiInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeFragmentViewModel @ViewModelInject constructor(
    application: Application,
    var apiInterface: ApiInterface
) :
    AndroidViewModel(application) {

    val isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData(false) }

    //Store server response into liveData
    val posts: MutableLiveData<ArrayList<Post>> by lazy { MutableLiveData() }
    val photos: ObservableArrayMap<Int, ArrayList<Photo>> by lazy { ObservableArrayMap() }
    val isPhotosLoading: ObservableArrayMap<Int, Boolean> by lazy { ObservableArrayMap() }

    /**
     * Getting posts from server and toast error when it's failed
     */
    fun getServerPosts() {
        posts.value = null
        isLoading.value = true
        val dispose = apiInterface.getPosts().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //Success
                isLoading.value = false
                posts.value = it
            }, {
                //Failed
                isLoading.value = false
                Toast.makeText(getApplication(), it.message, Toast.LENGTH_LONG).show()
            })
    }

    /**
     * Getting photos from server and toast error when it's failed
     */
    fun getServerImages(position: Int) {
        if (photos[position] == null) {
            isPhotosLoading[position] = true
            val postId = posts.value!![position].id!!
            val dispose = apiInterface.getPhotos(postId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //Success
                    isPhotosLoading[position] = false
                    photos[position] = ArrayList(it.filter { photo -> photo.albumId == 1 })
                }, {
                    //Failed
                    isPhotosLoading[position] = false
                    Toast.makeText(getApplication(), it.message, Toast.LENGTH_LONG).show()
                })
        }
    }
}