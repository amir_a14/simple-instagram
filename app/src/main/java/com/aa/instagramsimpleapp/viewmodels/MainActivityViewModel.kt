package com.aa.instagramsimpleapp.viewmodels

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel

class MainActivityViewModel @ViewModelInject constructor(
    application: Application
) :
    AndroidViewModel(application) {
}