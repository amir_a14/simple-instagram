package com.aa.instagramsimpleapp.viewmodels

import android.app.Application
import android.widget.Toast
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.aa.instagramsimpleapp.models.Comment
import com.aa.instagramsimpleapp.remote.ApiInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CommentsActivityViewModel @ViewModelInject constructor(
    application: Application,
    var apiInterface: ApiInterface
) :
    AndroidViewModel(application) {

    val isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData(false) }

    //Store server response into liveData
    val comments: MutableLiveData<ArrayList<Comment>> by lazy { MutableLiveData() }

    /**
     * Getting comments from server and toast error when it's failed
     */
    fun getServerComments(postId: Int) {
        isLoading.value = true
        val dispose = apiInterface.getComments(postId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //Success
                isLoading.value = false
                comments.value = it
            }, {
                //Failed
                isLoading.value = false
                Toast.makeText(getApplication(), it.message, Toast.LENGTH_LONG).show()
            })
    }
}