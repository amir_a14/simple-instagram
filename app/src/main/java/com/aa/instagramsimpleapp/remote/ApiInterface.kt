package com.aa.instagramsimpleapp.remote

import com.aa.instagramsimpleapp.models.Comment
import com.aa.instagramsimpleapp.models.Photo
import com.aa.instagramsimpleapp.models.Post
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("/posts")
    fun getPosts(): Observable<ArrayList<Post>>

    @GET("/posts/{id}/comments")
    fun getComments(@Path("id") postId: Int): Observable<ArrayList<Comment>>

    @GET("/posts/{id}/photos")
    fun getPhotos(@Path("id") postId: Int): Observable<ArrayList<Photo>>
}