package com.aa.instagramsimpleapp.models

data class Comment(
    var body: String? = null,
    var email: String? = null,
    var id: Int? = null,
    var name: String? = null,
    var postId: Int? = null
)