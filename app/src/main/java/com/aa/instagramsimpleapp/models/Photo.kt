package com.aa.instagramsimpleapp.models

data class Photo(
    var albumId: Int? = null,
    var id: Int? = null,
    var thumbnailUrl: String? = null,
    var title: String? = null,
    var url: String? = null
)