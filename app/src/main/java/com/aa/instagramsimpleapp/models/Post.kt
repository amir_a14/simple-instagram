package com.aa.instagramsimpleapp.models

import java.io.Serializable

data class Post(
    var body: String? = null,
    var id: Int? = null,
    var title: String? = null,
    var userId: Int? = null
) : Serializable